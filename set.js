(function() {
  
    var cardSelectMutLen = 2;
    
    var board = document.getElementById('board');
    
    var params = {
      
        'version': 0,
        'cardProps': {
    
            'diamond': 1,
            'squiggle': 2,
            'oval': 3,
            'striped': 1,
            'solid': 2,
            'open': 3,
            'blue': 1,
            'green': 2,
            'red': 3
          
        },
        'shape': 0,
        'fill': 1,
        'colour': 2,
        'number': 3,
        'delayRange': 0,
        'delayMin': 0,
        'totalTime': 0,
        'avgTime': 0,
        'samples': 0
    
    };
    
    function genDelay(range, min) {
      
    return Math.random()*range + min;
    
    }
    
    MutationObserver = window.MutationObserver || window.WebKitMutationObserver;
    
    var observer = new MutationObserver(function(mutations, observer) {
        
        if (mutations.length > cardSelectMutLen) {
          
            params.version = params.version + 1;
        
            console.log('triggered');
            
            var delay = genDelay(params.delayRange, params.delayMin);
            
            setTimeout(setup, delay, params);
        
        }
    });
    
    /*
    Define what element should be observed by the observer
    and what types of mutations trigger the callback
    */
    
    observer.observe(board, {
        subtree: true,
        childList: true
    });
    
    /*
    The following commented out code is an example of a simple brute force algorithm to find sets.
    If a set is found, it returns a list containing the three card objects that form the set.
    If it fails to find a set, it returns an empty list.
    */
    /*
    
    function verify(triple) {
      
        var shapeSum = 0;
        var colourSum = 0;
        var fillSum = 0;
        var numberSum = 0;
        
        for (var i = 0; i < triple.length; i++) {
            shapeSum += triple[i].shape;
            colourSum += triple[i].colour;
            fillSum += triple[i].fill;
            numberSum += triple[i].number;
        }
                
        var setCond = (!(shapeSum % 3) && !(colourSum % 3) && !(fillSum % 3) && !(numberSum % 3));
                
        if(setCond) {
            return true;
        }
        return false;
    }
    
    function findSet(cardList) {
      
        var triple = [];
          
        for(var i = 0; i < cardList.length; i++) {
            for(var j = 0; j < cardList.length; j++) {
              
            if(i == j) { continue; }
            
            for(var k = 0; k < cardList.length; k++) {
                    
                if(i == k || j == k) { continue; }
                
                var setCandidate = [cardList[i], cardList[j], cardList[k]];
                    
                res = verify(setCandidate);
                if(res) { return setCandidate; }
                }
            }
        }
        
        return triple;
    
    }
    
    */
    
    function Card(shape, fill, colour, number, card) {
        this.shape = shape;
        this.fill = fill;
        this.colour = colour;
        this.number = number;
        this.cardElem = card;
    }
    
    function findSet(cardList) {
      
        /*
        Write your algorithm here. Feel free to add new functions.
        There is no need to modify any of the other pre-existing functions.
        
        This function takes as input a set of card objects, and should behave as follows:
        If a set is found, return a list containing the three card objects that make up the set.
        If no set can be found, return an empty list.
        
        Card objects have the following structure:
        {
            'shape': int,       // the shape on the card
            'fill': int,    // the fill of the shape(s)
            'colour': int,      // the colour of the shape(s)
            'number': int,      // the number of shapes on the card
            'cardElem': object  // the DOM element corresponding to the card
        }
        
        */
        
        var set = [];
        return set;
      
    }
    
    function setup(params) {
      
        var cardProps = params.cardProps;
        var version = params.version;
          
        var rawCards = document.querySelectorAll('#board > tbody > tr > td');
        
        var cardList = [];
        
        for(var m = 0; m < rawCards.length; m++) {
            var card = rawCards[m];
            var imgs = card.getElementsByTagName('img');
            if(imgs && imgs.length > 0) {
                var src = imgs[0].getAttribute('src').split('/')[2].split('.')[0].split('_');
                
                var shape = cardProps[src[params.shape]];
                var fill = cardProps[src[params.fill]];
                var colour = cardProps[src[params.colour]];
                var number = imgs.length;
                var cardElem = card;
                
                var cardAttr = new Card(shape, fill, colour, number, cardElem);
                
                cardList.push(cardAttr);
            }
        }
        
        var start = new Date().getTime();
        var triple = findSet(cardList);
        var end = new Date().getTime();
        
        params.totalTime = params.totalTime + (end - start);
        params.samples = params.samples + 1;
        params.avgTime = params.totalTime/params.samples;
        
        if(triple.length) {
          
            for(var n = 0; n < triple.length; n++) {
                if(version != params.version) { return; }
                triple[n].cardElem.click();
            }
        }
        
        return;
    }
    
    var initDelay = genDelay(params.delayRange, params.delayMin);
            
    setTimeout(setup, initDelay, params);
    
})();